Example plain HTML site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/

---

Example plain HTML site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

<a href="https://www.wegroups.eu" title="studio rendering" >studio rendering</a><br>
<a href="https://www.wegroups.eu">studio 3d</a><br>
<a href="https://www.wegroups.eu/metaverso" title="metaverso" >metaverso</a><br>
<a href="https://www.wegroups.eu/rendering-interni" title="rendering interni" >rendering interni</a><br>
<a href="https://www.wegroups.eu/servizio-rendering-3d" title="rendering" >rendering</a><br>
<a href="https://www.wegroups.eu/modellazione-3d" title="modelli 3d" >modellazione 3d</a><br>
<a href="https://www.wegroups.eu/esperienze-immersive" title="esperienza immersiva" >esperienza immersiva</a><br>
<a href="https://www.wegroups.eu/showroom-virtuale" title="showroom virtuale" >showroom virtuale</a><br>
<a href="https://www.wegroups.eu/animazioni-3d" title="video 3d" >animazione 3d</a><br>
<a href="https://www.wegroups.eu/cose-la-cgi" title="CGI" >CGI</a><br>
<a href="https://www.dietistafaffini.it" title="nutrizionista arezzo"> nutrizionista arezzo</a><br>
<a href="https://www.wegroups.eu/motion-graphics" title="motion graphic" >motion graphics</a><br>
<a href="https://www.wegroups.eu/vr-automotive" title="vr automotive" >vr automotive</a><br>



## Troubleshooting

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
